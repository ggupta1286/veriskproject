package com.vormetric.app;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
public class DeTokenizeFilterDataFromDBOracle {
public static void main(String[] args) {
new DeTokenizeFilterDataFromDBOracle().DoIt();
}
@SuppressWarnings("null")
private void DoIt(){
try {
	TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }
        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }
        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
    }
};
SSLContext sc = null;
try {
	try {
		sc = SSLContext.getInstance("SSL");
	} catch (NoSuchAlgorithmException e) {
		e.printStackTrace();
	}
	sc.init(null, trustAllCerts, new java.security.SecureRandom());
} catch (KeyManagementException e) {
	e.printStackTrace();
}
HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
HostnameVerifier allHostsValid = new HostnameVerifier() {
    public boolean verify(String hostname, SSLSession session) {
        return true;
    }
};
HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
Connection cond2 = createNewConnection();
Connection cond1 = createNewConnection();
ResultSet rs = null;
ResultSet rsf = null;
ResultSet rsfg = null;
PreparedStatement preparedStatement2 = null;
PreparedStatement preparedStatement1 = null;
PreparedStatement preparedStatement3 = null;
PreparedStatement preparedStatement5 = null;
PreparedStatement preparedStatement6 = null;
String ccn[];
String ccndaily[];
String SQL_DROP_TABLE ="drop table tempperson";
try {
	 preparedStatement6 = cond2.prepareStatement(SQL_DROP_TABLE);
	 preparedStatement6.execute();
}catch(Exception e)
{
	System.out.println("Exception"+ e);
}
List rowValuestokenize = new ArrayList();
List rowValuestokenizede = new ArrayList();
String SQL_CREATE_TABLE = "CREATE GLOBAL TEMPORARY TABLE tempperson (lastname varchar2(80), firstname varchar2(80),dailyexpense number)  ON COMMIT PRESERVE ROWS";
List rowValues = new ArrayList();
List rowValuesln = new ArrayList();
List rowValuesde = new ArrayList();
try {
		 preparedStatement1 = cond2.prepareStatement(SQL_CREATE_TABLE);

		preparedStatement1.execute();
	String SQL_SELECT_DATA = "select FIRST_NAME,LAST_NAME,dailyexpensestr from employeetemp";
	 preparedStatement3 = cond2.prepareStatement(SQL_SELECT_DATA);
	rs = preparedStatement3.executeQuery();
		while (rs.next())
		{
			 rowValues.add(rs.getString("first_name"));
			 rowValuesln.add(rs.getString("last_name"));
			 rowValuesde.add(rs.getString("dailyexpensestr"));
		
		}
		rs.close();
		preparedStatement1.close();
		preparedStatement3.close();
		ccn = (String[]) rowValues.toArray(new String[rowValues.size()]);
		String[][] ra = chunkArray(ccn, 5000);
		for(int i=0;i<ra.length;i++)
		{
		String[] DeTokenValueFromDB = new detokenizefun().detokenize(ra[i]);
		for(int j=0;j<DeTokenValueFromDB.length;j++)
		{
			rowValuestokenize.add(DeTokenValueFromDB[j]);
		}
		};
		ccndaily = (String[]) rowValuesde.toArray(new String[rowValuesde.size()]);
		String[][] radaily = chunkArray(ccndaily, 5000);
		for(int i=0;i<radaily.length;i++)
		{
		String[] DeTokenValueFromDB = new detokenizefun().detokenize(radaily[i]);
		for(int j=0;j<DeTokenValueFromDB.length;j++)
		{
			rowValuestokenizede.add(Integer.parseInt(DeTokenValueFromDB[j]));
		}
		};
		for (int i=0;i<rowValuesln.size();i++) {
			String SQL_INSERT_DATA = "insert into  tempperson (firstname,lastname,dailyexpense) values (?,?,?)";			
				preparedStatement2 = cond2.prepareStatement(SQL_INSERT_DATA);
				preparedStatement2.setString(1, rowValuestokenize.get(i).toString());
				preparedStatement2.setString(2, rowValuesln.get(i).toString());
				preparedStatement2.setInt(3, (int)rowValuestokenizede.get(i));
					preparedStatement2.execute();
					preparedStatement2.close();
		}
	preparedStatement2.close();
String SQL_FILTER_DATA ="select firstname,lastname,dailyexpense as dailyexpense from tempperson where firstname like 'ram%'";
preparedStatement5 = cond2.prepareStatement(SQL_FILTER_DATA);
rsf = preparedStatement5.executeQuery();
while (rsf.next())
		{
		System.out.println("WildCard Search Data_______"+rsf.getString("firstname")+","+rsf.getString("lastname")+","+rsf.getInt("dailyexpense"));
		}

String SQL_FILTER_DATA_GROUP ="select lastname,sum(dailyexpense) as dailyexpense from tempperson where firstname like 'ram%' group by lastname";
preparedStatement6 = cond2.prepareStatement(SQL_FILTER_DATA_GROUP);
rsfg = preparedStatement6.executeQuery();
while (rsfg.next())
		{
		System.out.println("******GroupBy "+rsfg.getString("lastname")+"________"+rsfg.getString("lastname")+","+rsfg.getInt("dailyexpense"));
		
		
		}
	
} catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
 finally 
 {
	 try {
		
		        rs.close();
		       rsf.close();
		       rsfg.close();
		        preparedStatement1.close();
		        preparedStatement2.close();
		        preparedStatement3.close();
		        preparedStatement5.close();
		        preparedStatement6.close();
		     
		        cond1.close();
		        cond2.close();
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
 }
}
 catch (Exception e) {
e.printStackTrace();
}
}
private  Connection createNewConnection()
{
  Connection connection = null;

      try 
      {
    	connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "orcluser", "pawan123");
  }
  catch(SQLException sqle)
  {
      System.err.println("SQLException: "+sqle);
      return null;
  }

  return connection;
}
private String[][] chunkArray(String[] array, int chunkSize)
{
	int numOfChunks = (int)Math.ceil((double)array.length / chunkSize);
    String[][] output = new String[numOfChunks][];

    for(int i = 0; i < numOfChunks; ++i) {
        int start = i * chunkSize;
        int length = Math.min(array.length - start, chunkSize);

        String[] temp = new String[length];
        System.arraycopy(array, start, temp, 0, length);
        output[i] = temp;
    }
    return output;
}
    }
