package com.vormetric.app;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.codec.binary.Base64;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
public class VTSClientSample {
public static void main(String[] args) {
new VTSClientSample().DoIt();
}
@SuppressWarnings("null")
private void DoIt(){
String credential = Base64.encodeBase64String("vtsuser:Vtsuser@123".getBytes());
try {
	TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }
        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }
        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
    }
};
SSLContext sc = null;
try {
	try {
		sc = SSLContext.getInstance("SSL");
	} catch (NoSuchAlgorithmException e) {
		e.printStackTrace();
	}
	sc.init(null, trustAllCerts, new java.security.SecureRandom());
} catch (KeyManagementException e) {
	e.printStackTrace();
}
HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
HostnameVerifier allHostsValid = new HostnameVerifier() {
    public boolean verify(String hostname, SSLSession session) {
        return true;
    }
};
HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
String https_url = "https://3.101.44.220/vts/rest/v2.0/tokenize/";
URL myurl = new URL(https_url);
HttpsURLConnection con = (HttpsURLConnection)myurl.openConnection();
String ccNum = "9453677629008564";
String jStr =
"{\"data\":\""+ccNum+"\",\"tokengroup\":\"PC4_KEY\",\"tokentemplate\":\"navisitetemplate4\"}";
con.setRequestProperty("Content-length", String.valueOf(jStr.length()));
con.setRequestProperty("Content-Type","application/json");
con.setRequestProperty("Authorization","Basic "+credential);
con.setRequestMethod("POST");
con.setDoOutput(true);
con.setDoInput(true);
DataOutputStream output = new DataOutputStream(con.getOutputStream());
output.writeBytes(jStr);
output.close();
BufferedReader rd=new BufferedReader(new InputStreamReader(con.getInputStream()
));
String line = "";
String strResponse = "";
while ((line = rd.readLine()) != null) {
strResponse=strResponse+line;
}
rd.close();
con.disconnect();
System.out.println("Tokenize server: "+https_url);
System.out.println("Tokenize request: "+jStr);
System.out.println("Tokenize responsefinal: "+strResponse);
}
catch (MalformedURLException e) {
e.printStackTrace();
} catch (IOException e) {
e.printStackTrace();
}
}
}


